package ua.cn.stu.robobattleship.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ua.com.jbs.robobattleship.client.rest.RobobattleshipRest;

/**
 * Created by Victor Novik - novikvictor@gmail.com.
 */
@Configuration
public class RestConfig {
    @Bean
    @Scope(value = "singleton")
    public RobobattleshipRest client() {
        String serverUrl = System.getProperty("rb.server.url",
                "http://localhost:9999");
        return RobobattleshipRest
                        .connect(serverUrl);
    }
}
