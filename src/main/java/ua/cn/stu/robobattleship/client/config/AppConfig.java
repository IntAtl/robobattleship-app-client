package ua.cn.stu.robobattleship.client.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import ua.cn.stu.robobattleship.client.Main;
import ua.cn.stu.robobattleship.client.MyShootingCoordinateCalculator;
import ua.com.jbs.robobattleship.client.*;
import ua.com.jbs.robobattleship.client.rest.RobobattleshipRest;
import ua.com.jbs.robobattleship.client.rest.model.ResponseStatus;
import ua.com.jbs.robobattleship.client.rest.model.ShootResult;
import ua.com.jbs.robobattleship.client.rest.validator.ServerResponseValidator;

/**
 * Created by Victor Novik - novikvictor@gmail.com.
 */
@Configuration
@Import({ResponseValidatorConfig.class, RestConfig.class})
public class AppConfig {
    @Bean
    @Autowired
    public Main main(PlayerService playerService, BattleMapLoader mapLoader) {
        return new Main(playerService, mapLoader);
    }

    @Bean
    @Autowired
    public PlayerService playerService(
                    RobobattleshipRest client,
                    ServerResponseValidator<ResponseStatus> responseValidator,
                    ServerResponseValidator<ShootResult> shootValidator) {
        return new RobobattleshipPlayerService(client, responseValidator, shootValidator);
    }

    @Bean
    public ShootingCoordinateCalculator shootingCoordinateCalculator() {
        return new MyShootingCoordinateCalculator();
    }
    @Bean
    @Autowired
    public Battle battle(
                    ShootingCoordinateCalculator calculator,
                    PlayerService playerService,
                    ServerResponseValidator<ShootResult> serverResponseValidator) {
        return new RoboBattle(calculator, playerService, serverResponseValidator);
    }

    @Bean
    public BattleMapLoader battleGridLoader() {
        return new FileBattleMapLoader();
    }
}
