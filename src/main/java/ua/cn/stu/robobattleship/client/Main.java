package ua.cn.stu.robobattleship.client;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ua.cn.stu.robobattleship.client.config.AppConfig;
import ua.com.jbs.robobattleship.client.*;
import ua.com.jbs.robobattleship.client.rest.model.Player;

import java.util.Scanner;

/**
 * @author Victor Novik - novikvictor@gmail.com
 */
public class Main implements Logger {
    private PlayerService playerService;
    private BattleMapLoader battleMapLoader;

    public Main(
                    PlayerService playerService,
                    BattleMapLoader battleMapLoader) {
        this.playerService = playerService;
        this.battleMapLoader = battleMapLoader;
    }

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        String serverUrl = System.getProperty("rb.server.url", "http://localhost:9999");
        String playerName = System.getProperty("rb.player.name",
                        "changeMePls" + Long.toString(System.currentTimeMillis()));
        String playerMapLocation = System.getProperty("rb.player.map.location", "/battle_map.txt");

        MainArgumentsHolder argsHolder = new MainArgumentsHolder(serverUrl, playerName,
                        playerMapLocation);
        context.getBean(Main.class).initApp(argsHolder, context);
    }

    private void initApp(MainArgumentsHolder argsHolder, ApplicationContext context) {
        String playerBattleGrid = battleMapLoader.load(argsHolder.getPlayerMapLocation());

        Player player = playerService.setUpPlayer(argsHolder.getPlayerName(), playerBattleGrid);

        String readLine;
        try (Scanner consoleReader = new Scanner(System.in)) {

            while (consoleReader.hasNextLine() && !(readLine = readEnemyUid(consoleReader)).equals("exit")) {
                String enemyUid = StringUtils.trimToEmpty(readLine);
                if (StringUtils.isEmpty(enemyUid)) {
                    getLogger().error("Enemy uuid is empty");
                    continue;
                }

                context.getBean(Battle.class).doBattle(player, enemyUid);
            }

        }
        getLogger().info("Good bye...");
    }

    private String readEnemyUid(Scanner scanner) {
        getLogger().info("Enter enemy uid\n");
        return scanner.nextLine();
    }

    public static class MainArgumentsHolder {
        private String serverUrl;
        private String playerName;
        private String playerMapLocation;

        public MainArgumentsHolder(
                        String serverUrl,
                        String playerName,
                        String playerMapLocation) {
            this.serverUrl = serverUrl;
            this.playerName = playerName;
            this.playerMapLocation = playerMapLocation;
        }

        public String getServerUrl() {
            return this.serverUrl;
        }

        public String getPlayerName() {
            return this.playerName;
        }

        public String getPlayerMapLocation() {
            return playerMapLocation;
        }

        @Override
        public String toString() {
            return "MainArgumentsHolder{" + "serverUrl='" + serverUrl + '\'' + ", playerName='"
                            + playerName + '\'' + ", playerMapLocation='" + playerMapLocation + '\''
                            + '}';
        }
    }

}
