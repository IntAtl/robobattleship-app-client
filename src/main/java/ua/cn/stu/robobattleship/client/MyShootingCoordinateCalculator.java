package ua.cn.stu.robobattleship.client;

import ua.com.jbs.robobattleship.client.ShootingCoordinateCalculator;
import ua.com.jbs.robobattleship.client.model.EnemyGrid;
import ua.com.jbs.robobattleship.client.model.ShootCoordinates;
import ua.com.jbs.robobattleship.client.rest.model.ShootResult;

/**
 * Created by Victor Novik - novikvictor@gmail.com
 */
public class MyShootingCoordinateCalculator implements ShootingCoordinateCalculator {
    /**
     * Объект @class MyShootingCoordinateCalculator создается для каждого боя с противником.
     *
     * @param status - Статуст предыдущего выстрела
     * @param enemyShootingGrid - Карта попаданий по противнику
     * @return - Объект с координатами для боя
     */
    @Override
    public ShootCoordinates nextCoordinate(ShootResult.ShootResultEnum status, EnemyGrid enemyShootingGrid) {
        throw new UnsupportedOperationException("The method has not been implemented yet");
    }
}
