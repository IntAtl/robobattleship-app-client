package ua.cn.stu.robobattleship.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ua.com.jbs.robobattleship.client.rest.validator.GeneralResponseValidator;
import ua.com.jbs.robobattleship.client.rest.validator.ShootResponseValidator;

/**
 * Created by Victor Novik - novikvictor@gmail.com.
 */
@Configuration
public class ResponseValidatorConfig {

    @Bean
    public GeneralResponseValidator generalResponseValidator() {
        return new GeneralResponseValidator();
    }

    @Bean
    public ShootResponseValidator shootResponseValidator() {
        return new ShootResponseValidator(generalResponseValidator());
    }
}
